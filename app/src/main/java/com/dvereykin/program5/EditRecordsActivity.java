package com.dvereykin.program5;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Dmitry Vereykin aka eXrump on 9/29/2016.
 */


public class EditRecordsActivity extends Activity implements View.OnClickListener, ListView.OnItemClickListener, ListView.OnItemSelectedListener {
    Button addButton;
    Button editButton;
    Button deleteButton;
    Button viewButton;
    ListView listAddresses;

    final int ADDRESS_ENTRY = 1001;

    PersonalDataCollection addresses;
    PersonalDataArrayAdapter addressAdapter;
    DataSource addressData;
    int recordNumber = -1;
    boolean serviceConnectionBound = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_records);

        addButton = (Button) findViewById(R.id.add_button);
        deleteButton = (Button) findViewById(R.id.delete_button);
        editButton = (Button) findViewById(R.id.edit_button);
        viewButton = (Button) findViewById(R.id.view_button);

        addButton.setOnClickListener(this);
        deleteButton.setOnClickListener(this);
        editButton.setOnClickListener(this);
        viewButton.setOnClickListener(this);

        listAddresses = (ListView) findViewById(R.id.list_addresses);
        listAddresses.setOnItemClickListener(this);
        listAddresses.setOnItemSelectedListener(this);

        addressData = new DataSource(this);

        try {
            addressData.open();
            addresses = addressData.getAllAddresses();
            addressData.close();
        } catch (Exception e) {
            showMessage("Error loading address data.");
            addresses = new PersonalDataCollection();
        }

        addressAdapter = new PersonalDataArrayAdapter(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                addresses);
        listAddresses.setAdapter(addressAdapter);


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        PersonalDataIntent pdIntent = new PersonalDataIntent(data);
        if (requestCode == ADDRESS_ENTRY) {
            try {
                switch (resultCode) {
                    case RESULT_OK:
                        PDAttributeGroup address = new PDAttributeGroup(pdIntent.name,
                                pdIntent.address, pdIntent.city, pdIntent.state, pdIntent.zipCode);
                        switch (pdIntent.action) {
                            case ADD:
                                addressData.open();
                                address.id = addressData.createAddress(address);
                                addressData.close();
                                addresses.addAddress(address);
                                showMessage("Added");
                                break;
                            case DELETE:
                                addressData.open();
                                addressData.deleteAddress(addresses.getAddress(pdIntent.addressIndex));
                                addressData.close();
                                addresses.removeAddress(pdIntent.addressIndex);
                                showMessage("Deleted");
                                recordNumber = -1;
                                break;
                            case EDIT:
                                addressData.open();
                                addressData.deleteAddress(addresses.getAddress(pdIntent.addressIndex));
                                address.id = addressData.createAddress(address);
                                addressData.close();
                                addresses.setAddress(pdIntent.addressIndex, address);
                                showMessage("Updated");
                                break;
                            case VIEW:
                                showMessage("OK");
                                break;
                        }
                        addressAdapter.notifyDataSetChanged();


                        break;
                    case RESULT_CANCELED:
                        showMessage("Cancelled");
                        break;
                }
            } catch (Exception ex) {
                showError(ex);
                addressData.close();
            }
        }
    }

    public void onClick(View v) {
        PersonalDataIntent personalDataIntent;
        if (addButton.getId() == v.getId()) {
            if (!addresses.isAddressLimitReached()) {
                personalDataIntent = new PersonalDataIntent();
                personalDataIntent.action = PersonalDataIntent.ActionType.ADD;
                startActivityForResult(personalDataIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
            } else
                showMessage("Max addresses reached");
        } else {
            try {
                PDAttributeGroup address = addresses.getAddress(recordNumber);
                if (editButton.getId() == v.getId()) {
                    personalDataIntent = new PersonalDataIntent(address, PersonalDataIntent.ActionType.EDIT, recordNumber);
                    startActivityForResult(personalDataIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
                }
                if (deleteButton.getId() == v.getId()) {
                    personalDataIntent = new PersonalDataIntent(address, PersonalDataIntent.ActionType.DELETE, recordNumber);
                    startActivityForResult(personalDataIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
                }
                if (viewButton.getId() == v.getId()) {
                    personalDataIntent = new PersonalDataIntent(address, PersonalDataIntent.ActionType.VIEW, recordNumber);
                    startActivityForResult(personalDataIntent.getIntent(this, EditValuesActivity.class), ADDRESS_ENTRY);
                }
            } catch (Exception ex) {
                showError(ex);
            }
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onItemSelected(parent, view, position, id);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        recordNumber = position;
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    void showError(Exception message) {
        Toast.makeText(this, message.getMessage(), Toast.LENGTH_LONG).show();
    }

    void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            DataSource.ServiceBinder serviceBinder = (DataSource.ServiceBinder) service;
            addressData = serviceBinder.getService();
            addressData.initDataSource(EditRecordsActivity.this);
            serviceConnectionBound = true;

            try {
                addressData.open();
                addresses = addressData.getAllAddresses();
                addressData.close();

            } catch (Exception e) {
                showMessage("Error loading address data.");
                addresses = new PersonalDataCollection();
            }

            addressAdapter.addresses = addresses;
            addressAdapter.notifyDataSetChanged();
        }

        public void onServiceDisconnected(ComponentName arg0) {
            serviceConnectionBound = false;
        }

    };

}

class PersonalDataArrayAdapter extends ArrayAdapter<PDAttributeGroup> {
    private final Context context;
    public PersonalDataCollection addresses;

    public PersonalDataArrayAdapter(Context context, int resource,
                                    int textViewResourceId, PersonalDataCollection addresses) {
        super(context, resource, textViewResourceId, addresses.addressList);
        this.context = context;
        this.addresses = addresses;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PDAttributeGroup address = addresses.getAddress(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.layout_address_row, parent, false);

        TextView nameTextView = (TextView) rowView.findViewById(R.id.full_name);
        TextView addressTextView = (TextView) rowView.findViewById(R.id.full_address);

        nameTextView.setText(address.name);
        addressTextView.setText(address.address + " " + address.city +
                " " + address.state + " " + address.zipCode);

        return rowView;
    }

}

